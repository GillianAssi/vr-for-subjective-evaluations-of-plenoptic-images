	/************************************************************************************
 Filename    :   Win32_GLAppUtil.h
 Content     :   OpenGL and Application/Window setup functionality for RoomTiny
 Created     :   October 20th, 2014
 Author      :   Tom Heath
 Copyright   :   Copyright 2014 Oculus, LLC. All Rights reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 *************************************************************************************/

#ifndef OVR_Win32_GLAppUtil_h
#define OVR_Win32_GLAppUtil_h
 //shaders
#include "GL/CAPI_GLE.h"
#include "Extras/OVR_Math.h"
#include "OVR_CAPI_GL.h"
#include <assert.h>
#include <stb_image.h>
 // -------- Desk setup
#include <iostream>
#include <fstream>
#include <string>
#include <map>
using namespace std;
using namespace OVR;
std::string str_trim(std::string str);
float lum;



#ifndef VALIDATE
    #define VALIDATE(x, msg) if (!(x)) { MessageBoxA(NULL, (msg), "OculusRoomTiny", MB_ICONERROR | MB_OK); exit(-1); }
#endif

#ifndef OVR_DEBUG_LOG
    #define OVR_DEBUG_LOG(x)
#endif


//---------------------------------------------------------------------------------------
struct DepthBuffer
{
    GLuint        texId;
	
    DepthBuffer(Sizei size)
    {
        glGenTextures(1, &texId);
        glBindTexture(GL_TEXTURE_2D, texId);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        GLenum internalFormat = GL_DEPTH_COMPONENT24;
        GLenum type = GL_UNSIGNED_INT;
        if (GLE_ARB_depth_buffer_float)
        {
            internalFormat = GL_DEPTH_COMPONENT32F;
            type = GL_FLOAT;
        }

        glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, size.w, size.h, 0, GL_DEPTH_COMPONENT, type, NULL);
    }
    ~DepthBuffer()
    {
        if (texId)
        {
            glDeleteTextures(1, &texId);
            texId = 0;
        }
    }
};

//--------------------------------------------------------------------------
struct TextureBuffer
{
    GLuint              texId;
    GLuint              fboId;
    Sizei               texSize;
    TextureBuffer(bool rendertarget, Sizei size, int mipLevels, unsigned char * data) :
        texId(0),
        fboId(0),
        texSize(0, 0)
    {
        texSize = size;

        glGenTextures(1, &texId);
        glBindTexture(GL_TEXTURE_2D, texId);

        if (rendertarget)
        {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        }
        else
        {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        }

        glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB8_ALPHA8, texSize.w, texSize.h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

        if (mipLevels > 1)
        {
            glGenerateMipmap(GL_TEXTURE_2D);
        }

        if(rendertarget)
        {
            glGenFramebuffers(1, &fboId);
        }
    }

    ~TextureBuffer()
    {
        if (texId)
        {
            glDeleteTextures(1, &texId);
            texId = 0;
        }
        if (fboId)
        {
            glDeleteFramebuffers(1, &fboId);
            fboId = 0;
        }
    }

    Sizei GetSize() const
    {
        return texSize;
    }

    void SetAndClearRenderSurface(DepthBuffer* dbuffer)
    {
        VALIDATE(fboId, "Texture wasn't created as a render target");

        GLuint curTexId = texId;

        glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, curTexId, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, dbuffer->texId, 0);

        glViewport(0, 0, texSize.w, texSize.h);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_FRAMEBUFFER_SRGB);
    }

    void UnsetRenderSurface()
    {
        VALIDATE(fboId, "Texture wasn't created as a render target");

        glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0);
    }
};

//-------------------------------------------------------------------------------------------
struct OGL
{
    static const bool       UseDebugContext = false;

    HWND                    Window;
    HDC                     hDC;
    HGLRC                   WglContext;
    OVR::GLEContext         GLEContext;
    bool                    Running;
    bool                    Key[256];
    int                     WinSizeW;
    int                     WinSizeH;
    GLuint                  fboId;
    HINSTANCE               hInstance;

    static LRESULT CALLBACK WindowProc(_In_ HWND hWnd, _In_ UINT Msg, _In_ WPARAM wParam, _In_ LPARAM lParam)
    {
        OGL *p = reinterpret_cast<OGL *>(GetWindowLongPtr(hWnd, 0));
        switch (Msg)
        {
        case WM_KEYDOWN:
            p->Key[wParam] = true;
            break;
        case WM_KEYUP:
            p->Key[wParam] = false;
            break;
        case WM_DESTROY:
            p->Running = false;
            break;
        default:
            return DefWindowProcW(hWnd, Msg, wParam, lParam);
        }
        if ((p->Key['Q'] && p->Key[VK_CONTROL]) || p->Key[VK_ESCAPE])
        {
            p->Running = false;
        }
        return 0;
    }

    OGL() :
        Window(nullptr),
        hDC(nullptr),
        WglContext(nullptr),
        GLEContext(),
        Running(false),
        WinSizeW(0),
        WinSizeH(0),
        fboId(0),
        hInstance(nullptr)
    {
		// Clear input
		for (int i = 0; i < sizeof(Key) / sizeof(Key[0]); ++i)
            Key[i] = false;
    }

    ~OGL()
    {
        ReleaseDevice();
        CloseWindow();
    }

    bool InitWindow(HINSTANCE hInst, LPCWSTR title)
    {
        hInstance = hInst;
        Running = true;

        WNDCLASSW wc;
        memset(&wc, 0, sizeof(wc));
        wc.style = CS_CLASSDC;
        wc.lpfnWndProc = WindowProc;
        wc.cbWndExtra = sizeof(struct OGL *);
        wc.hInstance = GetModuleHandleW(NULL);
        wc.lpszClassName = L"ORT";
        RegisterClassW(&wc);

        // adjust the window size and show at InitDevice time
        Window = CreateWindowW(wc.lpszClassName, title, WS_OVERLAPPEDWINDOW, 0, 0, 0, 0, 0, 0, hInstance, 0);
        if (!Window) return false;

        SetWindowLongPtr(Window, 0, LONG_PTR(this));

        hDC = GetDC(Window);

        return true;
    }

    void CloseWindow()
    {
        if (Window)
        {
            if (hDC)
            {
                ReleaseDC(Window, hDC);
                hDC = nullptr;
            }
            DestroyWindow(Window);
            Window = nullptr;
            UnregisterClassW(L"OGL", hInstance);
        }
    }

    // Note: currently there is no way to get GL to use the passed pLuid
    bool InitDevice(int vpW, int vpH, const LUID* /*pLuid*/, bool windowed = true)
    {
        UNREFERENCED_PARAMETER(windowed);

        WinSizeW = vpW;
        WinSizeH = vpH;

        RECT size = { 0, 0, vpW, vpH };
        AdjustWindowRect(&size, WS_OVERLAPPEDWINDOW, false);
        const UINT flags = SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW;
        if (!SetWindowPos(Window, nullptr, 0, 0, size.right - size.left, size.bottom - size.top, flags))
            return false;

        PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARBFunc = nullptr;
        PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARBFunc = nullptr;
        {
            // First create a context for the purpose of getting access to wglChoosePixelFormatARB / wglCreateContextAttribsARB.
            PIXELFORMATDESCRIPTOR pfd;
            memset(&pfd, 0, sizeof(pfd));
            pfd.nSize = sizeof(pfd);
            pfd.nVersion = 1;
            pfd.iPixelType = PFD_TYPE_RGBA;
            pfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER;
            pfd.cColorBits = 32;
            pfd.cDepthBits = 16;
            int pf = ChoosePixelFormat(hDC, &pfd);
            VALIDATE(pf, "Failed to choose pixel format.");

            VALIDATE(SetPixelFormat(hDC, pf, &pfd), "Failed to set pixel format.");

            HGLRC context = wglCreateContext(hDC);
            VALIDATE(context, "wglCreateContextfailed.");
            VALIDATE(wglMakeCurrent(hDC, context), "wglMakeCurrent failed.");

            wglChoosePixelFormatARBFunc = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress("wglChoosePixelFormatARB");
            wglCreateContextAttribsARBFunc = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
            assert(wglChoosePixelFormatARBFunc && wglCreateContextAttribsARBFunc);

            wglDeleteContext(context);
        }

        // Now create the real context that we will be using.
        int iAttributes[] =
        {
            // WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
            WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
            WGL_COLOR_BITS_ARB, 32,
            WGL_DEPTH_BITS_ARB, 16,
            WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
            WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB, GL_TRUE,
            0, 0
        };

        float fAttributes[] = { 0, 0 };
        int   pf = 0;
        UINT  numFormats = 0;

        VALIDATE(wglChoosePixelFormatARBFunc(hDC, iAttributes, fAttributes, 1, &pf, &numFormats),
            "wglChoosePixelFormatARBFunc failed.");

        PIXELFORMATDESCRIPTOR pfd;
        memset(&pfd, 0, sizeof(pfd));
        VALIDATE(SetPixelFormat(hDC, pf, &pfd), "SetPixelFormat failed.");

        GLint attribs[16];
        int   attribCount = 0;
        if (UseDebugContext)
        {
            attribs[attribCount++] = WGL_CONTEXT_FLAGS_ARB;
            attribs[attribCount++] = WGL_CONTEXT_DEBUG_BIT_ARB;
        }

        attribs[attribCount] = 0;

        WglContext = wglCreateContextAttribsARBFunc(hDC, 0, attribs);
        VALIDATE(wglMakeCurrent(hDC, WglContext), "wglMakeCurrent failed.");

        OVR::GLEContext::SetCurrentContext(&GLEContext);
        GLEContext.Init();

        glGenFramebuffers(1, &fboId);

        glEnable(GL_DEPTH_TEST);
        glFrontFace(GL_CW);
        glEnable(GL_CULL_FACE);

        if (UseDebugContext && GLE_ARB_debug_output)
        {
            glDebugMessageCallbackARB(DebugGLCallback, NULL);
            if (glGetError())
            {
                OVR_DEBUG_LOG(("glDebugMessageCallbackARB failed."));
            }

            glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);

            // Explicitly disable notification severity output.
            glDebugMessageControlARB(GL_DEBUG_SOURCE_API, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, NULL, GL_FALSE);
        }

        return true;
    }

    bool HandleMessages(void)
    {
        MSG msg;
        while (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        return Running;
    }

    void Run(bool (*MainLoop)(bool retryCreate))
    {
        while (HandleMessages())
        {
            // true => we'll attempt to retry for ovrError_DisplayLost
            if (!MainLoop(true))
                break;
            // Sleep a bit before retrying to reduce CPU load while the HMD is disconnected
            Sleep(10);
        }
    }

    void ReleaseDevice()
    {
        if (fboId)
        {
            glDeleteFramebuffers(1, &fboId);
            fboId = 0;
        }
        if (WglContext)
        {
            wglMakeCurrent(NULL, NULL);
            wglDeleteContext(WglContext);
            WglContext = nullptr;
        }
        GLEContext.Shutdown();
    }

    static void GLAPIENTRY DebugGLCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
    {
        UNREFERENCED_PARAMETER(source);
        UNREFERENCED_PARAMETER(type);
        UNREFERENCED_PARAMETER(id);
        UNREFERENCED_PARAMETER(severity);
        UNREFERENCED_PARAMETER(length);
        UNREFERENCED_PARAMETER(message);
        UNREFERENCED_PARAMETER(userParam);
        OVR_DEBUG_LOG(("Message from OpenGL: %s\n", message));
    }
};

// Global OpenGL state
static OGL Platform;

//------------------------------------------------------------------------------
struct ShaderFill
{
    GLuint            program;
    TextureBuffer   * texture;

    ShaderFill(GLuint vertexShader, GLuint pixelShader, TextureBuffer* _texture)
    {
        texture = _texture;

        program = glCreateProgram();

        glAttachShader(program, vertexShader);
        glAttachShader(program, pixelShader);

        glLinkProgram(program);

        glDetachShader(program, vertexShader);
        glDetachShader(program, pixelShader);

        GLint r;
        glGetProgramiv(program, GL_LINK_STATUS, &r);
        if (!r)
        {
            GLchar msg[1024];
            glGetProgramInfoLog(program, sizeof(msg), 0, msg);
            OVR_DEBUG_LOG(("Linking shaders failed: %s\n", msg));
        }
    }

    ~ShaderFill()
    {
        if (program)
        {
            glDeleteProgram(program);
            program = 0;
        }
        if (texture)
        {
            delete texture;
            texture = nullptr;
        }
    }
};

//----------------------------------------------------------------
struct VertexBuffer
{
    GLuint    buffer;

    VertexBuffer(void* vertices, size_t size)
    {
        glGenBuffers(1, &buffer);
        glBindBuffer(GL_ARRAY_BUFFER, buffer);
        glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
    }
    ~VertexBuffer()
    {
        if (buffer)
        {
            glDeleteBuffers(1, &buffer);
            buffer = 0;
        }
    }
};

//----------------------------------------------------------------
struct IndexBuffer
{
    GLuint    buffer;

    IndexBuffer(void* indices, size_t size)
    {
        glGenBuffers(1, &buffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, indices, GL_STATIC_DRAW);
    }
    ~IndexBuffer()
    {
        if (buffer)
        {
            glDeleteBuffers(1, &buffer);
            buffer = 0;
        }
    }
};

//---------------------------------------------------------------------------
struct Model
{
    struct Vertex
    {
        Vector3f  Pos;
        DWORD     C;
        float     U, V;
    };

    Vector3f        Pos;
    Quatf           Rot;
    Matrix4f        Mat;
    int             numVertices, numIndices;
    Vertex          Vertices[2000]; // Note fixed maximum
    GLushort        Indices[2000];
    ShaderFill    * Fill;
	GLuint         Texture;
    VertexBuffer  * vertexBuffer;
    IndexBuffer   * indexBuffer;

    Model(Vector3f pos, ShaderFill * fill, GLuint texture) :
        numVertices(0),
        numIndices(0),
        Pos(pos),
        Rot(),
        Mat(),
        Fill(fill),
		Texture(texture),
        vertexBuffer(nullptr),
        indexBuffer(nullptr)
    {}

    ~Model()
    {
        FreeBuffers();
    }

    Matrix4f& GetMatrix()
    {
        Mat = Matrix4f(Rot);
        Mat = Matrix4f::Translation(Pos) * Mat;
        return Mat;
    }

    void AddVertex(const Vertex& v) { Vertices[numVertices++] = v; }
    void AddIndex(GLushort a) { Indices[numIndices++] = a; }

    void AllocateBuffers()
    {
        vertexBuffer = new VertexBuffer(&Vertices[0], numVertices * sizeof(Vertices[0]));
        indexBuffer = new IndexBuffer(&Indices[0], numIndices * sizeof(Indices[0]));
    }

    void FreeBuffers()
    {
        delete vertexBuffer; vertexBuffer = nullptr;
        delete indexBuffer; indexBuffer = nullptr;
    }

    void AddSolidColorBox(float x1, float y1, float z1, float x2, float y2, float z2, DWORD c)
    {
        Vector3f Vert[][2] =
        {
            Vector3f(x1, y2, z1), Vector3f(z1, x1), Vector3f(x2, y2, z1), Vector3f(z1, x2),
            Vector3f(x2, y2, z2), Vector3f(z2, x2), Vector3f(x1, y2, z2), Vector3f(z2, x1),
            Vector3f(x1, y1, z1), Vector3f(z1, x1), Vector3f(x2, y1, z1), Vector3f(z1, x2),
            Vector3f(x2, y1, z2), Vector3f(z2, x2), Vector3f(x1, y1, z2), Vector3f(z2, x1),
            Vector3f(x1, y1, z2), Vector3f(z2, y1), Vector3f(x1, y1, z1), Vector3f(z1, y1),
            Vector3f(x1, y2, z1), Vector3f(z1, y2), Vector3f(x1, y2, z2), Vector3f(z2, y2),
            Vector3f(x2, y1, z2), Vector3f(z2, y1), Vector3f(x2, y1, z1), Vector3f(z1, y1),
            Vector3f(x2, y2, z1), Vector3f(z1, y2), Vector3f(x2, y2, z2), Vector3f(z2, y2),
            Vector3f(x1, y1, z1), Vector3f(x1, y1), Vector3f(x2, y1, z1), Vector3f(x2, y1),
            Vector3f(x2, y2, z1), Vector3f(x2, y2), Vector3f(x1, y2, z1), Vector3f(x1, y2),
            Vector3f(x1, y1, z2), Vector3f(x1, y1), Vector3f(x2, y1, z2), Vector3f(x2, y1),
            Vector3f(x2, y2, z2), Vector3f(x2, y2), Vector3f(x1, y2, z2), Vector3f(x1, y2)
        };

        GLushort CubeIndices[] =
        {
            0, 1, 3, 3, 1, 2,
            5, 4, 6, 6, 4, 7,
            8, 9, 11, 11, 9, 10,
            13, 12, 14, 14, 12, 15,
            16, 17, 19, 19, 17, 18,
            21, 20, 22, 22, 20, 23
        };

        for (int i = 0; i < sizeof(CubeIndices) / sizeof(CubeIndices[0]); ++i)
            AddIndex(CubeIndices[i] + GLushort(numVertices));

        // Generate a quad for each box face
        for (int v = 0; v < 6 * 4; v++)
        {
            // Make vertices, with some token lighting
            Vertex vvv; vvv.Pos = Vert[v][0]; vvv.U = Vert[v][1].x; vvv.V = Vert[v][1].y;
            float dist1 = (vvv.Pos - Vector3f(-2, 4, -2)).Length();
            float dist2 = (vvv.Pos - Vector3f(3, 4, -3)).Length();
            float dist3 = (vvv.Pos - Vector3f(-4, 3, 25)).Length();
            int   bri = rand() % 160;
            float B = ((c >> 16) & 0xff) * (bri + 192.0f * (0.65f + 8 / dist1 + 1 / dist2 + 4 / dist3)) / 255.0f;
            float G = ((c >>  8) & 0xff) * (bri + 192.0f * (0.65f + 8 / dist1 + 1 / dist2 + 4 / dist3)) / 255.0f;
            float R = ((c >>  0) & 0xff) * (bri + 192.0f * (0.65f + 8 / dist1 + 1 / dist2 + 4 / dist3)) / 255.0f;
            vvv.C = (c & 0xff000000) +
                ((R > 255 ? 255 : DWORD(R)) << 16) +
                ((G > 255 ? 255 : DWORD(G)) << 8) +
                 (B > 255 ? 255 : DWORD(B));
            AddVertex(vvv);
        }
    }
	//________________________________________________________________

	//------------------------ START NEW CODE ------------------------
	//________________________________________________________________
	void AddSolidColorBox(float x, float y, float z, float length, float heigth, float depth, DWORD c, Model * m)
	{
		float x2 = x + length;
		float y2 = y + heigth;
		float z2 = z - depth;
		m->AddSolidColorBox(x, y, z2, x2, y2, z, c);
	}
	void Addtable(float h, float l, float d, float x, float y, float z, DWORD c, Model * m)
	{
		float tblT = 0.025f; // table thickness
		float legT = 0.05f; // leg thickness
		m->AddSolidColorBox((x - (l / 2)), (y + h - tblT), (z + (d / 2)), l, tblT, d, c, m);
		m->AddSolidColorBox((x - (l / 2)), y, (z - (d / 2) + legT), legT, (h - tblT), legT, c, m); // front right Table Leg 
		m->AddSolidColorBox((x + (l / 2) - legT), y, (z - (d / 2) + legT), legT, (h - tblT), legT, c, m); // front left Table Leg 
		m->AddSolidColorBox((x + (l / 2) - legT), y, (z + (d / 2)), legT, (h - tblT), legT, c, m); // back left Table Leg 
		m->AddSolidColorBox((x - (l / 2)), y, (z + (d / 2)), legT, (h - tblT), legT, c, m); // back right Table Leg 
	}
	void Addchair(float h, float l, float d, float x, float y, float z, DWORD c, Model * m)
	{
		float chrT = 0.025f; // chair thickness
		float legT = 0.05f; // leg thickness
		m->AddSolidColorBox((x - (l / 2)), ((y + h) / 2 - chrT), (z + (d / 2)), l, chrT, d, c, m); // Chair Set
		m->AddSolidColorBox((x - (l / 2)), y, (z - (d / 2) + legT), legT, (h / 2 - chrT), legT, c, m); // Chair Leg 1
		m->AddSolidColorBox((x + (l / 2) - legT), y, (z - (d / 2) + legT), legT, (h / 2 - chrT), legT, c, m); // front left Chair Leg 
		m->AddSolidColorBox((x + (l / 2) - legT), y, (z + (d / 2)), legT, (h / 2 - chrT), legT, c, m); // back left Chair Leg 
		m->AddSolidColorBox((x - (l / 2)), y, (z + (d / 2)), legT, (h / 2 - chrT), legT, c, m); // back right Chair Leg 
		m->AddSolidColorBox((x - (l / 2)), ((y + h) / 2), (z - (d / 2) + chrT), l, (h / 2 + 0.1f), chrT, c, m); // Chair Back 
	}

	void Addscreen(float h, float l, float d, float x, float y, float z, DWORD c, Model * m)
	{
		m->AddSolidColorBox((x - (l / 2)), (y - (h / 2)), (z - (d / 2)), l, h, d, c, m); // Screen
	}
	void AddSetup(float scrh, float scrw, float scrd, float scrth, float deskh, float deskl, float deskd, float x, float y, float z, Model * m)
	{
		m->Addtable(deskh, deskl, deskd, x, y, z, 0xff505050, m); // table
		m->Addchair(1.0f, 0.48f, 0.53f, x, y, z - 0.53f, 0xff111111, m); //chair
		m->Addscreen((scrw*0.625f), scrw, scrth, x, (scrw*0.625f / 2.0f + deskh + scrh), (z - (deskd / 2) + scrd), 0xff111111, m); // screen ratio 10:16 = 0.625
	}
	//________________________________________________________________

	//------------------------- END NEW CODE -------------------------
	//________________________________________________________________
    void Render(Matrix4f view, Matrix4f proj)
    {
        Matrix4f combined = proj * view * GetMatrix();

        glUseProgram(Fill->program);
        glUniform1i(glGetUniformLocation(Fill->program, "Texture0"), 0);
        glUniformMatrix4fv(glGetUniformLocation(Fill->program, "matWVP"), 1, GL_TRUE, (FLOAT*)&combined);
		glUniform1f(glGetUniformLocation(Fill->program, "Lum"), lum); //change luminance

        glActiveTexture(GL_TEXTURE0);
		//________________________________________________________________

		//------------------------ START NEW CODE ------------------------
		//________________________________________________________________
		if (Texture != NULL) glBindTexture(GL_TEXTURE_2D, Texture);
		else glBindTexture(GL_TEXTURE_2D, Fill->texture->texId);
		//________________________________________________________________

		//------------------------- END NEW CODE -------------------------
		//________________________________________________________________
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer->buffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer->buffer);

        GLuint posLoc = glGetAttribLocation(Fill->program, "Position");
        GLuint colorLoc = glGetAttribLocation(Fill->program, "Color");
        GLuint uvLoc = glGetAttribLocation(Fill->program, "TexCoord");


        glEnableVertexAttribArray(posLoc);
        glEnableVertexAttribArray(colorLoc);
        glEnableVertexAttribArray(uvLoc);

        glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)OVR_OFFSETOF(Vertex, Pos));
        glVertexAttribPointer(colorLoc, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*)OVR_OFFSETOF(Vertex, C));
        glVertexAttribPointer(uvLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)OVR_OFFSETOF(Vertex, U));

        glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_SHORT, NULL);

        glDisableVertexAttribArray(posLoc);
        glDisableVertexAttribArray(colorLoc);
        glDisableVertexAttribArray(uvLoc);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        glUseProgram(0);
    }
	//________________________________________________________________

	//------------------------ START NEW CODE ------------------------
	//________________________________________________________________
	// add triangle
	void AddTriangle()
	{
		// triangle vertices
		Vector3f TriangleVertices[][2] =
		{
			Vector3f(1.0f, 0.0f, -2.0f), Vector3f(0.0f, 0.0f),
			Vector3f(2.0f, 0.0f, -2.0f), Vector3f(1.0f, 0.0f),
			Vector3f(1.5f, 1.0f, -2.0f), Vector3f(0.5f, 1.0f)
		};

		// triangle indices
		GLushort TriangleIndices[] =
		{
			0, 1, 2,
			2, 1, 0
		};
		// GLushort = unsigned binary integer --> GL_UNSIGNED_SHORT

		// add indices
		int indexCount = sizeof(TriangleIndices) / sizeof(TriangleIndices[0]);
		for (int i = 0; i < indexCount; ++i)
			AddIndex(TriangleIndices[i] + GLushort(numVertices));

		// add vertices
		int vertexCount = sizeof(TriangleVertices) / sizeof(TriangleVertices[0]);
		for (int v = 0; v < vertexCount; v++)
		{
			Vertex vertex;

			vertex.Pos = TriangleVertices[v][0];
			vertex.U = TriangleVertices[v][1].x;
			vertex.V = TriangleVertices[v][1].y;
			//vertex.C = 0xff202050;

			AddVertex(vertex);
		}
	}
	void AddRectangle(float x1, float y1, float z1, float x2, float y2, float z2)
	{
		// Rectangle vertices
		Vector3f RectangleVertices[][2] =
		{
			Vector3f(x1, y1, z1), Vector3f(1.0f, 1.0f), //LO
			Vector3f(x2, y1, z2), Vector3f(0.0f, 1.0f), //RO
			Vector3f(x1, y2, z1), Vector3f(1.0f, 0.0f), //LB
			Vector3f(x2, y2, z2), Vector3f(0.0f, 0.0f) //RB
		};
		// Rectangle indices
		GLushort RectangleIndices[] =
		{
			0, 2, 3,
			0, 3, 1,
			1, 3, 0,
			3, 2, 0
		};
		// GLushort = unsigned binary integer --> GL_UNSIGNED_SHORT

		// add indices
		int indexCount = sizeof(RectangleIndices) / sizeof(RectangleIndices[0]);
		for (int i = 0; i < indexCount; ++i)
			AddIndex(RectangleIndices[i] + GLushort(numVertices));

		// add vertices
		int vertexCount = sizeof(RectangleVertices) / sizeof(RectangleVertices[0]);
		for (int v = 0; v < vertexCount; v++)
		{
			Vertex vertex;

			vertex.Pos = RectangleVertices[v][0];
			vertex.U = RectangleVertices[v][1].x;
			vertex.V = RectangleVertices[v][1].y;
			//vertex.C = 0xff202050; // comment this out because I will put a texture on it instead of a color

			AddVertex(vertex);
		}
	}

//________________________________________________________________

//------------------------- END NEW CODE -------------------------
//________________________________________________________________
};

//------------------------------------------------------------------------- 
struct Scene
{
    int     numModels;
    Model * Models[10];

    void    Add(Model * n)
    {
        Models[numModels++] = n;
    }

    void Render(Matrix4f view, Matrix4f proj)
    {
        for (int i = 0; i < numModels; ++i)
            Models[i]->Render(view, proj);
    }

    GLuint CreateShader(GLenum type, const GLchar* src)
    {
        GLuint shader = glCreateShader(type);

        glShaderSource(shader, 1, &src, NULL);
        glCompileShader(shader);

        GLint r;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &r);
        if (!r)
        {
            GLchar msg[1024];
            glGetShaderInfoLog(shader, sizeof(msg), 0, msg);
            if (msg[0]) {
                OVR_DEBUG_LOG(("Compiling shader failed: %s\n", msg));
            }
            return 0;
        }

        return shader;
    }

    void Init(int includeIntensiveGPUobject)
    {
		//________________________________________________________________

		//------------------------ START NEW CODE ------------------------
		//________________________________________________________________
		//------------ Read Setup File --------------
		string line;
		std::map<string, float> variables;
		ifstream myfile("Setup.txt");
		if (myfile.is_open())
		{
			while (getline(myfile, line))
			{
				unsigned long  index = line.find("=");
				std::string token = line.substr(0, index);
				line.erase(0, index + 1);

				variables.insert(std::make_pair(str_trim(token), std::stof(line)));
			}
			myfile.close();
		}
		// give the variables the demanded values
		else cout << "Unable to open file";
		float scrw = variables["scrw"];
		float scrh = variables["scrh"];
		float scrdist = variables["scrdist"];
		float scrth = variables["scrth"];
		float deskh = variables["deskh"];
		float deskl = variables["deskl"];
		float deskd = variables["deskd"];
		lum = variables["luminance"];
		//________________________________________________________________

		//------------------------- END NEW CODE -------------------------
		//________________________________________________________________
		static const GLchar* VertexShaderSrc = R"glsl(
			#version 150
            uniform mat4 matWVP;
			uniform float Lum;
            in      vec4 Position;
            in      vec4 Color;
            in      vec2 TexCoord;
            out     vec2 oTexCoord;
            out     vec4 oColor;
            void main()
            {
               gl_Position = (matWVP * Position);
               oTexCoord   = TexCoord;
               oColor.rgb  = pow(Color.rgb, vec3(2.2))* Lum;   // convert from sRGB to linear
               oColor.a    = Color.a;
            }
			)glsl";
			static const char* FragmentShaderSrc = R"glsl(
            #version 150
            uniform sampler2D Texture0;
            in      vec4      oColor;
            in      vec2      oTexCoord;
            out     vec4      FragColor;
            void main()
            {
               FragColor = oColor * texture2D(Texture0, oTexCoord);
            }
			)glsl";
        GLuint    vshader = CreateShader(GL_VERTEX_SHADER, VertexShaderSrc);
        GLuint    fshader = CreateShader(GL_FRAGMENT_SHADER, FragmentShaderSrc);

        // Make textures
        ShaderFill * grid_material[5];
        for (int k = 0; k < 4; ++k)
        {
            static DWORD tex_pixels[256 * 256];
            for (int j = 0; j < 256; ++j)
            {
                for (int i = 0; i < 256; ++i)
                {
                    if (k == 0) tex_pixels[j * 256 + i] = (((i >> 7) ^ (j >> 7)) & 1) ? 0xffb4b4b4 : 0xff505050;// floor
                    if (k == 1) tex_pixels[j * 256 + i] = (((j / 4 & 15) == 0) || (((i / 4 & 15) == 0) && ((((i / 4 & 31) == 0) ^ ((j / 4 >> 4) & 1)) == 0)))
                        ? 0xff3c3c3c : 0xffb4b4b4;// wall
                    if (k == 2) tex_pixels[j * 256 + i] = (i / 4 == 0 || j / 4 == 0) ? 0xff505050 : 0xffb4b4b4;// ceiling
                    if (k == 3) tex_pixels[j * 256 + i] = 0xffffffff;// blank
                }
            }
            TextureBuffer * generated_texture = new TextureBuffer(false, Sizei(256, 256), 4, (unsigned char *)tex_pixels);
            grid_material[k] = new ShaderFill(vshader, fshader, generated_texture);
        }
		//________________________________________________________________

		//------------------------ START NEW CODE ------------------------
		//________________________________________________________________
		 // Bind lightfield shader
		ShaderFill * Lightfieldmaterial;
			static DWORD tex_pixels[256 * 256];
			for (int j = 0; j < 256; ++j)
			{
				for (int i = 0; i < 256; ++i)
				{
					tex_pixels[j * 256 + i] = (((i >> 7) ^ (j >> 7)) & 1) ? 0xffb4b4b4 : 0xff505050;
				}
			TextureBuffer * generated_texture = new TextureBuffer(false, Sizei(256, 256), 4, (unsigned char *)tex_pixels);
			Lightfieldmaterial = new ShaderFill(vshader, fshader, generated_texture);
		}
		//-------------
		GLuint texture[2];
		glGenTextures(1, &texture[0]);
		glBindTexture(GL_TEXTURE_2D, texture[0]);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		int width, height, nrChannels;
		unsigned char *image = stbi_load("pic.png", &width, &height, &nrChannels, 0);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);

		stbi_image_free(image);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture[0]);
		//-----------
		glGenTextures(1, &texture[1]);
		glBindTexture(GL_TEXTURE_2D, texture[1]);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		image = stbi_load("wood.jpg", &width, &height, &nrChannels, 0);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);

		stbi_image_free(image);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture[1]);
		glDeleteShader(vshader);
		glDeleteShader(fshader);

		// Construct  geometry
		//Setup
		Model * m = new Model(Vector3f(0, 0, 0), grid_material[1], texture[1]);
		m->AddSetup(scrh, scrw, scrdist, scrth, deskh, deskl, deskd, 0.0f, 0.0f, 0.0f, m); // desk setup
		m->AllocateBuffers();
		Add(m);
		//screen
		m = new Model(Vector3f(0, 0, 0), grid_material[1], texture[0]);
		// the coordinates folow the logic of the desk setup function so the displayed texture is on the screen;
		// the +0.0001 in the z axis is an offset so the texture of the screen won't glitch with the texture of the display
		// the 0.0f represent the original x y z position of the "addsetup" function, this way I can easely change it for a variable if i want to be able to change the position of the desk
		m->AddRectangle((0.0f - (scrw / 2.0f)+ 0.015f), (deskh + scrh + 0.015f), (0.0f - deskd/2.0f + scrdist - 3.0f/2.0f * scrth-0.0001f), (0.0f + scrw / 2.0f - 0.015f), (deskh + scrh + scrw * 0.625f - 0.015f), (0.0f - deskd / 2.0f + scrdist - 3.0f / 2.0f * scrth - 0.0001f));
		m->AllocateBuffers();
		Add(m);
		// Rating system
		m = new Model(Vector3f(0, 0, 0), grid_material[1], texture[0]);
		m->AddRectangle((0.0f - (scrw / 2.0f)), (deskh + scrh + scrw * 0.625f + 0.01f), (0.0f - deskd / 2.0f + scrdist - 3.0f / 2.0f * scrth), (0.0f + scrw / 2.0f - 0.015f), (deskh + scrh + scrw * 0.625f + 0.11f), (0.0f - deskd / 2.0f + scrdist - 3.0f / 2.0f * scrth));
		m->AllocateBuffers();
		Add(m);
		//________________________________________________________________

		//------------------------- END NEW CODE -------------------------
		//________________________________________________________________

        m = new Model(Vector3f(0, 0, 0), grid_material[1], NULL);  // Walls
        m->AddSolidColorBox(-2.1f, 0.0f, -2.1f, -2.0f, 4.0f, 2.0f, 0xff424242); // Right Wall
        m->AddSolidColorBox(-2.0f, 0.0f, -2.1f, 2.1f, 4.0f, -2.0f, 0xff424242); // Back Wall
        m->AddSolidColorBox(2.0f, 0.0f, -2.0f, 2.1f, 4.0f, 2.0f, 0xff424242); // Left Wall
		m->AddSolidColorBox(-2.1f, -0.0f, 2.0f, 2.1f, 4.0f, 2.1f, 0xff424242); // Front Wall
        m->AllocateBuffers();
        Add(m);

        if (includeIntensiveGPUobject)
        {
            m = new Model(Vector3f(0, 0, 0), grid_material[0], NULL);  // Floors
            for (float depth = 0.0f; depth > -3.0f; depth -= 0.1f)
                m->AddSolidColorBox(9.0f, 0.5f, -depth, -9.0f, 3.5f, -depth, 0xff424242); // Partition
            m->AllocateBuffers();
            Add(m);
        }

        m = new Model(Vector3f(0, 0, 0), grid_material[0], texture[1]);  // Floors
        m->AddSolidColorBox(-2.0f, -0.1f, -2.0f, 2.0f, 0.0f, 2.0f, 0xff604633); // Main floor
        m->AllocateBuffers();
        Add(m);

        m = new Model(Vector3f(0, 0, 0), grid_material[2], NULL);  // Ceiling
        m->AddSolidColorBox(-2.0f, 4.0f, -2.0f, 2.0f, 4.0f, 2.0f, 0xff424242);
        m->AllocateBuffers();
        Add(m);
    }

    Scene() : numModels(0) {}
    Scene(bool includeIntensiveGPUobject) :
        numModels(0)
    {
        Init(includeIntensiveGPUobject);
    }
    void Release()
    {
        while (numModels-- > 0)
            delete Models[numModels];
    }
    ~Scene()
    {
        Release();
    }
	//------- To trim from spaces --------
	std::string str_trim(std::string str)
	{
		std::string output = "";
		for (unsigned long i = 0; i < str.length(); i++)
		{
			if (str[i] != ' ') output += str[i];
		}
		return output;
	}
};

#endif // OVR_Win32_GLAppUtil_h
