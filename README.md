Author:     Gillian Assi    
University: VUB     
Faculty:    Industrial Engeneering in Electronics and ICT.

//---------------------------------------------------------

This project is written in Visual Studio 2017 using OpenGL. Oculus needs to be
installed to use the OculusAPK.

Each directory contains a number. This corresponds to the update of my update.
The directory with the highest number is the newest version. 
Next to the number is described what the update contained.

You will always find the .sln file by following this path:
...\Samples\OculusRoomTiny\OculusRoomTiny (GL)\Projects\Windows\VS2017

Note that an Oculus Rift or Oculus Rift S headset is required to run the
application.
If you wish to run this application without an HMD do the following steps:

1.  opened the SDK Tools folder
2.  run the OculusDebugTool.exe file
3.  change the Debug HMD Type to CV1 
4.  Done. A window should pop up of the virtual world.

Disclaimer: No controls are forseen to move up and down. This will affect the hologram.