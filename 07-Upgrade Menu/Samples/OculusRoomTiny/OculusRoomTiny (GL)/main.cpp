/*****************************************************************************

Filename    :   main.cpp
Content     :   Simple minimal VR demo
Created     :   December 1, 2014
Author      :   Tom Heath
Copyright   :   Copyright (c) Facebook Technologies, LLC and its affiliates. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

/*****************************************************************************/
/// This sample has not yet been fully assimiliated into the framework
/// and also the GL support is not quite fully there yet, hence the VR
/// is not that great!

#include "../../OculusRoomTiny_Advanced/Common/Win32_GLAppUtil.h"
//________________________________________________________________

//------------------------ START NEW CODE ------------------------
//________________________________________________________________
#include "windows.h"
#include <vector>
//image loading
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <sys/types.h>
//file searching
#include <dirent.h>
#include <algorithm>
//datlogging & reading
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <time.h>  
// gl math
/*#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>*/
//________________________________________________________________

//------------------------- END NEW CODE -------------------------
//________________________________________________________________
// Include the Oculus SDK
#include "OVR_CAPI_GL.h"

#if defined(_WIN32)
    #include <dxgi.h> // for GetDefaultAdapterLuid
    #pragma comment(lib, "dxgi.lib")
#endif
using namespace std;
using namespace OVR;
//________________________________________________________________

//------------------------ START NEW CODE ------------------------
//________________________________________________________________
bool RightHandAButtonPressed = false;
int load = 0;
int menudelay = 0;
int start = 1;
GLuint Ratingtexture[20];
GLuint Screentexture[10];
int Rating = 0;
int ScreenIm = 0;
int offset = 0;
int totalIm = 0;
//data logging
#define DTTMFMT "Date:%Y-%m-%d;Time:%H:%M:%S;"
#define DTTMSZ 80
vector<string> ImageNames;

std::string str_trim(std::string str);
//________________________________________________________________

//------------------------- END NEW CODE -------------------------
//________________________________________________________________
struct OculusTextureBuffer
{
    ovrSession          Session;
    ovrTextureSwapChain ColorTextureChain;
    ovrTextureSwapChain DepthTextureChain;
    GLuint              fboId;
    Sizei               texSize;

    OculusTextureBuffer(ovrSession session, Sizei size, int sampleCount) :
        Session(session),
        ColorTextureChain(nullptr),
        DepthTextureChain(nullptr),
        fboId(0),
        texSize(0, 0)
    {
        assert(sampleCount <= 1); // The code doesn't currently handle MSAA textures.

        texSize = size;

        // This texture isn't necessarily going to be a rendertarget, but it usually is.
        assert(session); // No HMD? A little odd.

        ovrTextureSwapChainDesc desc = {};
        desc.Type = ovrTexture_2D;
        desc.ArraySize = 1;
        desc.Width = size.w;
        desc.Height = size.h;
        desc.MipLevels = 1;
        desc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
        desc.SampleCount = sampleCount;
        desc.StaticImage = ovrFalse;

        {
            ovrResult result = ovr_CreateTextureSwapChainGL(Session, &desc, &ColorTextureChain);

            int length = 0;
            ovr_GetTextureSwapChainLength(session, ColorTextureChain, &length);

            if(OVR_SUCCESS(result))
            {
                for (int i = 0; i < length; ++i)
                {
                    GLuint chainTexId;
                    ovr_GetTextureSwapChainBufferGL(Session, ColorTextureChain, i, &chainTexId);
                    glBindTexture(GL_TEXTURE_2D, chainTexId);

                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
                }
            }
        }

        desc.Format = OVR_FORMAT_D32_FLOAT;

        {
            ovrResult result = ovr_CreateTextureSwapChainGL(Session, &desc, &DepthTextureChain);

            int length = 0;
            ovr_GetTextureSwapChainLength(session, DepthTextureChain, &length);

            if (OVR_SUCCESS(result))
            {
              for (int i = 0; i < length; ++i)
              {
                GLuint chainTexId;
                ovr_GetTextureSwapChainBufferGL(Session, DepthTextureChain, i, &chainTexId);
                glBindTexture(GL_TEXTURE_2D, chainTexId);

                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
              }
            }
        }

        glGenFramebuffers(1, &fboId);
    }

    ~OculusTextureBuffer()
    {
        if (ColorTextureChain)
        {
            ovr_DestroyTextureSwapChain(Session, ColorTextureChain);
            ColorTextureChain = nullptr;
        }
        if (DepthTextureChain)
        {
            ovr_DestroyTextureSwapChain(Session, DepthTextureChain);
            DepthTextureChain = nullptr;
        }
        if (fboId)
        {
            glDeleteFramebuffers(1, &fboId);
            fboId = 0;
        }
    }

    Sizei GetSize() const
    {
        return texSize;
    }
    void SetAndClearRenderSurface()
    {
        GLuint curColorTexId;
        GLuint curDepthTexId;
        {
            int curIndex;
            ovr_GetTextureSwapChainCurrentIndex(Session, ColorTextureChain, &curIndex);
            ovr_GetTextureSwapChainBufferGL(Session, ColorTextureChain, curIndex, &curColorTexId);
        }
        {
          int curIndex;
          ovr_GetTextureSwapChainCurrentIndex(Session, DepthTextureChain, &curIndex);
          ovr_GetTextureSwapChainBufferGL(Session, DepthTextureChain, curIndex, &curDepthTexId);
        }

        glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, curColorTexId, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, curDepthTexId, 0);

        glViewport(0, 0, texSize.w, texSize.h);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_FRAMEBUFFER_SRGB);
    }

    void UnsetRenderSurface()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0);
    }

    void Commit()
    {
        ovr_CommitTextureSwapChain(Session, ColorTextureChain);
        ovr_CommitTextureSwapChain(Session, DepthTextureChain);
    }
};

static ovrGraphicsLuid GetDefaultAdapterLuid()
{
    ovrGraphicsLuid luid = ovrGraphicsLuid();

    #if defined(_WIN32)
        IDXGIFactory* factory = nullptr;

        if (SUCCEEDED(CreateDXGIFactory(IID_PPV_ARGS(&factory))))
        {
            IDXGIAdapter* adapter = nullptr;

            if (SUCCEEDED(factory->EnumAdapters(0, &adapter)))
            {
                DXGI_ADAPTER_DESC desc;

                adapter->GetDesc(&desc);
                memcpy(&luid, &desc.AdapterLuid, sizeof(luid));
                adapter->Release();
            }

            factory->Release();
        }
    #endif

    return luid;
}


static int Compare(const ovrGraphicsLuid& lhs, const ovrGraphicsLuid& rhs)
{
    return memcmp(&lhs, &rhs, sizeof(ovrGraphicsLuid));
}
//________________________________________________________________

//------------------------ START NEW CODE ------------------------
//________________________________________________________________
// this function takes all the file in the directory and puts them in a vector
void getDir(const char* d, vector<string> & files)
{
	FILE* pipe = NULL;
	string pCmd = "dir /B " + string(d);
	char buf[256];

	if (NULL == (pipe = _popen(pCmd.c_str(), "rt")))
	{
		cout << "Path not found" << endl;
		return;
	}

	while (!feof(pipe))
	{
		if (fgets(buf, 256, pipe) != NULL)
		{
			files.push_back(string(buf));
		}

	}

	_pclose(pipe);
}
// this function puts all  the textures from a file in texture and returns its pointer
GLuint *GetTextures(const char* directory, GLuint texture[]) {
	vector<string> filenames;
	getDir(directory, filenames);
	std::string s = directory;
	for (unsigned int i = 0; i < filenames.size(); i++)
	{
		filenames[i].erase(std::remove_if(filenames[i].begin(), filenames[i].end(), ::isspace), filenames[i].end());
	}
	if (s == "Screen") { totalIm = filenames.size(); ImageNames = filenames; } //this way we know how many images we can change line 566 + we know the names of the files

	for (unsigned int i = 0; i < filenames.size(); i++) 
	{
		glActiveTexture(GL_TEXTURE0 + offset + i);
		
		string path = s + "\\" + filenames[i];
		glGenTextures(1, &texture[i]);
		glBindTexture(GL_TEXTURE_2D, texture[i]);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		int width, height, nrChannels;
		unsigned char *image = stbi_load(path.c_str(), &width, &height, &nrChannels, 0);//path.c_str() is to change it to a unsigned char
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);

		stbi_image_free(image);
		glBindTexture(GL_TEXTURE_2D, offset + i);
		glActiveTexture(GL_TEXTURE0 + offset + i);
		glBindTexture(GL_TEXTURE_2D, texture[i]);
	}
	offset += filenames.size();
	return texture;
}
//this function is ti get the date and time
static char *getDtTm(char *buff) {
	time_t t = time(0);
	struct tm time_info;
	localtime_s(&time_info, &t);
	strftime(buff, DTTMSZ, DTTMFMT, &time_info);
	return buff;
}
//________________________________________________________________

//------------------------- END NEW CODE -------------------------
//________________________________________________________________
// return true to retry later (e.g. after display lost)
static bool MainLoop(bool retryCreate)
{
	//________________________________________________________________

	//------------------------ START NEW CODE ------------------------
	//________________________________________________________________
	GLuint *texture1;
	GLuint *texture2;
	// data logging
	srand((unsigned int)time(NULL)); 
	char buff[DTTMSZ];
	ofstream file;
	bool error = false;
	// timer
	clock_t this_time = clock();
	clock_t last_time = this_time;
	clock_t time_counter = 0;
	//rating
	int TimeVieuwed = 0;
	Vector3f RatePos;
	bool rateperm = false;
	bool rate = false;
	string line;
	std::map<string, float> variables;
	ifstream myfile("Menu.txt");
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			unsigned long  index = line.find("=");
			std::string token = line.substr(0, index);
			line.erase(0, index + 1);

			variables.insert(std::make_pair(str_trim(token), std::stof(line)));
		}
		myfile.close();
	}
	// give the variables the demanded values
	else cout << "Unable to open file";
	int stars = (int)variables["stars"];
	float delayM = variables["delayM"];
	float RateTime = variables["RateTime"];
	int rateNext = 0;
	//head tracking
	Vector3f HMDpos;
	float yaw = 0, pitch = 0, roll = 0;
	//________________________________________________________________

	//------------------------- END NEW CODE -------------------------
	//________________________________________________________________
	OculusTextureBuffer * eyeRenderTexture[2] = { nullptr, nullptr };
	ovrMirrorTexture mirrorTexture = nullptr;
	GLuint          mirrorFBO = 0;
	Scene         * roomScene = nullptr;
	long long frameIndex = 0;
    ovrSession session;
    ovrGraphicsLuid luid;
    ovrResult result = ovr_Create(&session, &luid);
    if (!OVR_SUCCESS(result))
        return retryCreate;
    if (Compare(luid, GetDefaultAdapterLuid())) // If luid that the Rift is on is not the default adapter LUID...
    {
        VALIDATE(false, "OpenGL supports only the default graphics adapter.");
    }

    ovrHmdDesc hmdDesc = ovr_GetHmdDesc(session);

    // Setup Window and Graphics
    // Note: the mirror window can be any size, for this sample we use 1/2 the HMD resolution
    ovrSizei windowSize = { hmdDesc.Resolution.w / 2, hmdDesc.Resolution.h / 2 };
    if (!Platform.InitDevice(windowSize.w, windowSize.h, reinterpret_cast<LUID*>(&luid)))
        goto Done;

    // Make eye render buffers
    for (int eye = 0; eye < 2; ++eye)
    {
        ovrSizei idealTextureSize = ovr_GetFovTextureSize(session, ovrEyeType(eye), hmdDesc.DefaultEyeFov[eye], 1);
        eyeRenderTexture[eye] = new OculusTextureBuffer(session, idealTextureSize, 1);

        if (!eyeRenderTexture[eye]->ColorTextureChain || !eyeRenderTexture[eye]->DepthTextureChain)
        {
            if (retryCreate) goto Done;
            VALIDATE(false, "Failed to create texture.");
        }
    }

    ovrMirrorTextureDesc desc;
    memset(&desc, 0, sizeof(desc));
    desc.Width = windowSize.w;
    desc.Height = windowSize.h;
    desc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;

    // Create mirror texture and an FBO used to copy mirror texture to back buffer
    result = ovr_CreateMirrorTextureWithOptionsGL(session, &desc, &mirrorTexture);
    if (!OVR_SUCCESS(result))
    {
        if (retryCreate) goto Done;
        VALIDATE(false, "Failed to create mirror texture.");
    }

    // Configure the mirror read buffer
    GLuint texId;
    ovr_GetMirrorTextureBufferGL(session, mirrorTexture, &texId);

    glGenFramebuffers(1, &mirrorFBO);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, mirrorFBO);
    glFramebufferTexture2D(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texId, 0);
    glFramebufferRenderbuffer(GL_READ_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

    // Turn off vsync to let the compositor do its magic
    wglSwapIntervalEXT(0);

    // Make scene - can simplify further if needed
    roomScene = new Scene(false);
    // FloorLevel will give tracking poses where the floor height is 0
    ovr_SetTrackingOriginType(session, ovrTrackingOrigin_FloorLevel);
    
    // Main loop
	while (Platform.HandleMessages())
	{
		//end new code
		this_time = clock();
		time_counter += this_time - last_time;
		last_time = this_time;
		// END new code
		ovrSessionStatus sessionStatus;
		ovr_GetSessionStatus(session, &sessionStatus);
		if (sessionStatus.ShouldQuit)
		{
			// Because the application is requested to quit, should not request retry
			retryCreate = false;
			break;
		}
		if (sessionStatus.ShouldRecenter)
			ovr_RecenterTrackingOrigin(session);

		if (sessionStatus.IsVisible)
		{
			// Keyboard inputs to adjust player orientation
			static float Yaw(3.141592f);
			if (Platform.Key[VK_LEFT])  Yaw += 0.02f;
			if (Platform.Key[VK_RIGHT]) Yaw -= 0.02f;

			// Keyboard inputs to adjust player position
			static Vector3f Pos2(0.0f, 0.0f, -0.865f);
			if (Platform.Key['W'] || Platform.Key[VK_UP])     Pos2 += Matrix4f::RotationY(Yaw).Transform(Vector3f(0, 0, -0.01f));
			if (Platform.Key['S'] || Platform.Key[VK_DOWN])   Pos2 += Matrix4f::RotationY(Yaw).Transform(Vector3f(0, 0, +0.01f));
			if (Platform.Key['D'])                            Pos2 += Matrix4f::RotationY(Yaw).Transform(Vector3f(+0.01f, 0, 0));
			if (Platform.Key['A'])                            Pos2 += Matrix4f::RotationY(Yaw).Transform(Vector3f(-0.01f, 0, 0));



			// Call ovr_GetRenderDesc each frame to get the ovrEyeRenderDesc, as the returned values (e.g. HmdToEyePose) may change at runtime.
			ovrEyeRenderDesc eyeRenderDesc[2];
			eyeRenderDesc[0] = ovr_GetRenderDesc(session, ovrEye_Left, hmdDesc.DefaultEyeFov[0]);
			eyeRenderDesc[1] = ovr_GetRenderDesc(session, ovrEye_Right, hmdDesc.DefaultEyeFov[1]);

			// Get eye poses, feeding in correct IPD offset
			ovrPosef EyeRenderPose[2];
			ovrPosef HmdToEyePose[2] = { eyeRenderDesc[0].HmdToEyePose,
										 eyeRenderDesc[1].HmdToEyePose };

			double sensorSampleTime;    // sensorSampleTime is fed into the layer later
			ovr_GetEyePoses(session, frameIndex, ovrTrue, HmdToEyePose, EyeRenderPose, &sensorSampleTime);

			ovrTimewarpProjectionDesc posTimewarpProjectionDesc = {};

			// Render Scene to Eye Buffers
			for (int eye = 0; eye < 2; ++eye)
			{
				// Switch to eye render target
				eyeRenderTexture[eye]->SetAndClearRenderSurface();

				// Get view and projection matrices
				Matrix4f rollPitchYaw = Matrix4f::RotationY(Yaw);
				Matrix4f finalRollPitchYaw = rollPitchYaw * Matrix4f(EyeRenderPose[eye].Orientation);
				Vector3f finalUp = finalRollPitchYaw.Transform(Vector3f(0, 1, 0));
				Vector3f finalForward = finalRollPitchYaw.Transform(Vector3f(0, 0, -1));
				Vector3f shiftedEyePos = Pos2 + rollPitchYaw.Transform(EyeRenderPose[eye].Position);

				Matrix4f view = Matrix4f::LookAtRH(shiftedEyePos, shiftedEyePos + finalForward, finalUp);
				Matrix4f proj = ovrMatrix4f_Projection(hmdDesc.DefaultEyeFov[eye], 0.2f, 1000.0f, ovrProjection_None);
				posTimewarpProjectionDesc = ovrTimewarpProjectionDesc_FromProjection(proj, ovrProjection_None);

				// Render world
				roomScene->Render(view, proj);

				// Avoids an error when calling SetAndClearRenderSurface during next iteration.
				// Without this, during the next while loop iteration SetAndClearRenderSurface
				// would bind a framebuffer with an invalid COLOR_ATTACHMENT0 because the texture ID
				// associated with COLOR_ATTACHMENT0 had been unlocked by calling wglDXUnlockObjectsNV.
				eyeRenderTexture[eye]->UnsetRenderSurface();

				// Commit changes to the textures so they get picked up frame
				eyeRenderTexture[eye]->Commit();
			}
			// Do distortion rendering, Present and flush/sync

			ovrLayerEyeFovDepth ld = {};
			ld.Header.Type = ovrLayerType_EyeFovDepth;
			ld.Header.Flags = ovrLayerFlag_TextureOriginAtBottomLeft;   // Because OpenGL.
			ld.ProjectionDesc = posTimewarpProjectionDesc;
			ld.SensorSampleTime = sensorSampleTime;

			for (int eye = 0; eye < 2; ++eye)
			{
				ld.ColorTexture[eye] = eyeRenderTexture[eye]->ColorTextureChain;
				ld.DepthTexture[eye] = eyeRenderTexture[eye]->DepthTextureChain;
				ld.Viewport[eye] = Recti(eyeRenderTexture[eye]->GetSize());
				ld.Fov[eye] = hmdDesc.DefaultEyeFov[eye];
				ld.RenderPose[eye] = EyeRenderPose[eye];
			}

			ovrLayerHeader* layers = &ld.Header;
			result = ovr_SubmitFrame(session, frameIndex, nullptr, &layers, 1);
			// exit the rendering loop if submit returns an error, will retry on ovrError_DisplayLost
			if (!OVR_SUCCESS(result))
				goto Done;

			frameIndex++;
		}
		//________________________________________________________________

		//------------------------ START NEW CODE ------------------------ Load the textures after the roomscene is rendered 
		//																   Otherwhise glActiveTexture(GL_TEXTURE0 + i); will give a nullpointer
		//________________________________________________________________
		if (start == 1)
		{
			texture1 = GetTextures("Rating", Ratingtexture);
			*Ratingtexture = *texture1;
			texture2 = GetTextures("Screen", Screentexture);
			*Screentexture = *texture2;
			RatePos = roomScene->Models[2]->Pos;		//I NEED TO KNOW WHERE THE ORIGINAL POSITION OF THE RATING BAR IS
			roomScene->Models[1]->Texture = Screentexture[ScreenIm];
			roomScene->Models[2]->Texture = Ratingtexture[Rating];
			roomScene->Models[2]->Pos = Vector3f(10.0f, 10.0f, 10.0f); //out of sight
			start = 0;
		}
		// thismakes it possible to use the buttons of the controller
		// determine whether left hand high
		double displayMidpointSeconds = ovr_GetPredictedDisplayTime(session, frameIndex);
		ovrTrackingState trackState = ovr_GetTrackingState(session, displayMidpointSeconds, ovrTrue);
		
		//ovrPosef leftHandPose = trackState.HandPoses[ovrHand_Left].ThePose;
		Posef HeadPose = trackState.HeadPose.ThePose;

		//every second
		if (time_counter > CLOCKS_PER_SEC || error)
		{
			HeadPose.Rotation.GetEulerAngles<Axis_Y, Axis_X, Axis_Z>(&yaw, &pitch, &roll);
			HMDpos = trackState.HeadPose.ThePose.Position;
			file.open("HeadsetDataLog.txt", ios::app);
			file << getDtTm(buff) << "Xpos:" << HMDpos[0] << ";Ypos:" << HMDpos[1] << ";Zpos:" << HMDpos[2] << ";Yaw:" << yaw << ";Pitch:" << pitch << "Roll:" << roll << ";Error:" << error << std::endl;
			file.close();
			if (!error)
			{
				time_counter -= CLOCKS_PER_SEC; TimeVieuwed += 1;
				if (rate) { rateNext += 1; }
			}
			if (error) { error = false; }
		}
		if (TimeVieuwed >= delayM) { rateperm = true; }
		if (rateperm && Platform.Key[VK_RETURN] && menudelay < 1 && !rate) { rate = true; roomScene->Models[2]->Pos = RatePos; menudelay = 20;}
		// this is the menu
		if (Platform.Key[VK_BACK] && menudelay < 1) { error = true; menudelay = 20; }
		if (Platform.Key['N'] && menudelay < 1 && Rating < stars-1 && rate) { Rating += 1; menudelay = 20; roomScene->Models[2]->Texture = Ratingtexture[Rating];}
		if (Platform.Key['B'] && menudelay < 1 && Rating > 0 && rate) { Rating -= 1; menudelay = 20; roomScene->Models[2]->Texture = Ratingtexture[Rating];}//VK_RETURN
		if ((Platform.Key[VK_RETURN] && menudelay < 1 && rate)||(rate && rateNext >= RateTime)) 
		{ 
			ScreenIm += 1; 
			if (ScreenIm > totalIm-1) { ScreenIm = 0; }
			menudelay = 20; 
			file.open("Datalog.txt", ios::app);
			file << getDtTm(buff) << "image:" << ImageNames[ScreenIm].c_str() << ";TimeVieuwed(s):" << TimeVieuwed << ";rating:" << Rating + 1 << std::endl;
			file.close();
			TimeVieuwed = 0;
			rateNext = 0;
			rateperm = false; rate = false;
			roomScene->Models[1]->Texture = Screentexture[ScreenIm];
			roomScene->Models[2]->Pos = Vector3f(10.0f,10.0f,10.0f); //out of sight
		}
		if (menudelay > 0)													menudelay -= 1;
		// oculus controllers
		ovrInputState    inputState;
		bool leftHandHigh = false;
		// determine whether left hand trigger pressed
		bool leftHandTriggerPressed = false;
		if (OVR_SUCCESS(ovr_GetInputState(session, ovrControllerType_Touch, &inputState)))
		{
			if (inputState.Buttons &ovrButton_A && load < 1)
			{
				if (RightHandAButtonPressed)
				{
					RightHandAButtonPressed = false;
					load = 10;
				}
				else
				{
					RightHandAButtonPressed = true;
					load = 10;
				}
			}
			if (inputState.HandTrigger[ovrHand_Left] > 0.5f)
			{
				leftHandTriggerPressed = true;
			}

		}
		// set haptic feedback vibrations
		if (leftHandHigh && leftHandTriggerPressed) {
			ovr_SetControllerVibration(session, ovrControllerType_LTouch, 0.0f, 1.0f);
		}
		else {
			ovr_SetControllerVibration(session, ovrControllerType_LTouch, 0.0f, 0.0f);
		}
		if (RightHandAButtonPressed)
		{
			ovr_SetControllerVibration(session, ovrControllerType_RTouch, 0.0f, 1.0f);
			if (load > 0)
			{
				load = load - 1;
			}
		}
		else
		{
			ovr_SetControllerVibration(session, ovrControllerType_RTouch, 0.0f, 0.0f);
			if (load > 0)
			{
				load = load - 1;
			}
		}
		//________________________________________________________________

		//------------------------- END NEW CODE -------------------------
		//________________________________________________________________
        // Blit mirror texture to back buffer
        glBindFramebuffer(GL_READ_FRAMEBUFFER, mirrorFBO);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        GLint w = windowSize.w;
        GLint h = windowSize.h;
        glBlitFramebuffer(0, h, w, 0,
                          0, 0, w, h,
                          GL_COLOR_BUFFER_BIT, GL_NEAREST);
        glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

        SwapBuffers(Platform.hDC);
    }

Done:
    delete roomScene;
    if (mirrorFBO) glDeleteFramebuffers(1, &mirrorFBO);
    if (mirrorTexture) ovr_DestroyMirrorTexture(session, mirrorTexture);
    for (int eye = 0; eye < 2; ++eye)
    {
        delete eyeRenderTexture[eye];
    }
    Platform.ReleaseDevice();
    ovr_Destroy(session);

    // Retry on ovrError_DisplayLost
    return retryCreate || (result == ovrError_DisplayLost);

}

//-------------------------------------------------------------------------------------
int WINAPI WinMain(HINSTANCE hinst, HINSTANCE, LPSTR, int)
{
    // Initializes LibOVR, and the Rift
	ovrInitParams initParams = { ovrInit_RequestVersion | ovrInit_FocusAware, OVR_MINOR_VERSION, NULL, 0, 0 };
	ovrResult result = ovr_Initialize(&initParams);
    VALIDATE(OVR_SUCCESS(result), "Failed to initialize libOVR.");

    VALIDATE(Platform.InitWindow(hinst, L"Oculus Room Tiny (GL)"), "Failed to open window.");

    Platform.Run(MainLoop);

    ovr_Shutdown();

    return(0);
}
	std::string str_trim(std::string str)
	{
		std::string output = "";
		for (unsigned long i = 0; i < str.length(); i++)
		{
			if (str[i] != ' ') output += str[i];
		}
		return output;
	}

